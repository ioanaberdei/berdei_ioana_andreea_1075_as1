const FIRST_NAME = "Ioana-Andreea";
const LAST_NAME = "Berdei";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function numberParser(value) {
  if (typeof value === "string") return parseInt(Number(value));
  else if (typeof value === "number") {
    if (
      Number.isFinite(value) === false ||
      value > Number.MAX_SAFE_INTEGER ||
      value < Number.MIN_SAFE_INTEGER ||
      value === NaN
    )
      return NaN;
    else return parseInt(value);
  }
}

module.exports = {
  FIRST_NAME,
  LAST_NAME,
  GRUPA,
  numberParser
};
